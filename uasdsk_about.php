<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title></title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body class="body">
		<div class="header">
			<div class="container-header">
				<h1><b>F BLOG!</b></h1>
				<p><b>This is all About me</b></p>
				<div class="jam-digital-f">
					<div class="kotak">
						<p id="jam"></p>
					</div>
					<div class="kotak">
						<p id="menit"></p>
					</div>
					<div class="kotak">
						<p id="detik"></p>
					</div>
					<script>
						window.setTimeout("waktu()", 1000);
					 
						function waktu() {
							var waktu = new Date();
							setTimeout("waktu()", 1000);
							document.getElementById("jam").innerHTML = waktu.getHours();
							document.getElementById("menit").innerHTML = waktu.getMinutes();
							document.getElementById("detik").innerHTML = waktu.getSeconds();
						}
					</script>
				</div>
			</div>			
			<div class="container-photo">
				<img src="./foto jas almamater.jpg" alt="">
			</div>
		</div>
		<div class="badan">	
			<div class="sidebar">
				<h1>Menu</h1>
				<ul>
					<li><a class ="button1" href="uasdsk_home.php">Beranda</a></li>
					<li><a class ="button1" href="uasdsk_profile.php">Profil Akademik</a></li>
					<li><a class ="button1" href="uasdsk_cv.php">Curriculum Vitae</a></li>
					<li><a class ="button1" href="uasdsk_about.php">Tentang</a></li>		
					<li><a class ="button1" href="uasdsk_contacts.php">Kontak</a></li>
				</ul>
			</div>
			<div class="content p">
				<p>Ada sedikit cerita menarik sebelum aku menjadi mahasiswa Teknik Informatika. Aku sudah memiliki ketertarikan terhadap komputer dan perkembangan teknologi informasi sejak usia yang
					masih terbilang muda, karena aku sudah penasaran tentang bagaimana cara komputer itu bekerja sejak di kelas 1 sekolah dasar. Pada masa itu aku sudah memiliki sebuah laptop yang performanya cukup
					baik untuk keperluan tugas sekolah. Dan beranjak SMP dan SMA aku juga semakin ingin tahu lebih dalam bagaimana teknologi berkembang begitu cepat dari masa ke masa dan bersyukurnya aku juga memiliki
					teman-teman dekat yang satu frekuensi denganku yang memiliki ketertarikan dan minat lebih terhadap komputer.</p>
				<p>Pada semester 1 ini aku punya dua mata kuliah yang bisa dibilang menjadi favoritku yaitu Pengantar Teknologi Informasi dan 
					Organisasi Arsitektur Komputer pada dua mata kuliah ini aku menemukan ketertarikan lebih pada bidang Informatika. Aku lebih banyak mengerti tentang bagian-bagian komputer dan
					sistem kerjanya yang sebelumnya belum aku ketahui dan ini memiliki daya tarik sendiri untukkku. Lebih kurangnya seperti inilah cerita pengalaman pada kuliah pertamaku! :)</p>
			</div>
		</div>
		<div class="clear"></div>
		<div class="footer">
		<?php
            echo "Muhammad Fahmi @2022 (footer ini dibuat dengan fitur PHP)";
        ?> 
		</div>