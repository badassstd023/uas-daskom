<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title></title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body class="body">
		<div class="header">
			<div class="container-header">
				<h1><b>F BLOG!</b></h1>
				<p><b>This my Contacts</b></p>
				<div class="jam-digital-f">
					<div class="kotak">
						<p id="jam"></p>
					</div>
					<div class="kotak">
						<p id="menit"></p>
					</div>
					<div class="kotak">
						<p id="detik"></p>
					</div>
					<script>
						window.setTimeout("waktu()", 1000);
					 
						function waktu() {
							var waktu = new Date();
							setTimeout("waktu()", 1000);
							document.getElementById("jam").innerHTML = waktu.getHours();
							document.getElementById("menit").innerHTML = waktu.getMinutes();
							document.getElementById("detik").innerHTML = waktu.getSeconds();
						}
					</script>
				</div>
			</div>			
			<div class="container-photo">
				<img src="./foto jas almamater.jpg" alt="">
			</div>
			
			
		</div>
		<div class="badan">			
			<div class="sidebar">
				<h1>Menu</h1>
				<ul>
					<li><a class ="button1" href="uasdsk_home.php">Beranda</a></li>
					<li><a class ="button1" href="uasdsk_profile.php">Profil Akademik</a></li>
					<li><a class ="button1" href="uasdsk_cv.php">Curriculum Vitae</a></li>
					<li><a class ="button1" href="uasdsk_about.php">Tentang</a></li>		
					<li><a class ="button1" href="uasdsk_contacts.php">Kontak</a></li>
				</ul>
			</div>
			<div class="content">
				<ul class="l">
					<li><a class ="button1" href="https://web.whatsapp.com/wa.me/+6285161522302"><span class="fa fa-whatsapp"></span> WhatsApp</a></li>
					<li><a class ="button1" href="https://instagram.com/mffahmii_"><span class="fa fa-instagram"></span> Instagram</a></li>
					<li><a class ="button1" href="https://twitter.com/mhdfahmi23"><span class="fa fa-twitter"></span> Twitter</a></li>
					<li><a class ="button1" href="mailto:111202214523@mhs.dinus.ac.id"><span class="fa fa-at"></span> E-mail</a></li>		
				</ul>
			</div>
		</div>
		<div class="clear"></div>
		<div class="footer">
		<?php
            echo "Muhammad Fahmi @2022 (footer ini dibuat dengan fitur PHP)";
        ?> 
		</div>
	
</body>
</html>