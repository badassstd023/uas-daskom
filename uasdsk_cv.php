<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title></title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body class="body">
		<div class="header">
			<div class="container-header">
				<h1><b>F BLOG!</b></h1>
				<p><b>This is my Curriculum Vitae</b></p>
				<div class="jam-digital-f">
					<div class="kotak">
						<p id="jam"></p>
					</div>
					<div class="kotak">
						<p id="menit"></p>
					</div>
					<div class="kotak">
						<p id="detik"></p>
					</div>
					<script>
						window.setTimeout("waktu()", 1000);
					 
						function waktu() {
							var waktu = new Date();
							setTimeout("waktu()", 1000);
							document.getElementById("jam").innerHTML = waktu.getHours();
							document.getElementById("menit").innerHTML = waktu.getMinutes();
							document.getElementById("detik").innerHTML = waktu.getSeconds();
						}
					</script>
				</div>
			</div>			
			<div class="container-photo">
				<img src="./foto jas almamater.jpg" alt="">
			</div>
			
			
		</div>
		<div class="badan">			
			<div class="sidebar">
				<h1>Menu</h1>
				<ul>
					<li><a class ="button1" href="uasdsk_home.php">Beranda</a></li>
					<li><a class ="button1" href="uasdsk_profile.php">Profil Akademik</a></li>
					<li><a class ="button1" href="uasdsk_cv.php">Curriculum Vitae</a></li>
					<li><a class ="button1" href="uasdsk_about.php">Tentang</a></li>		
					<li><a class ="button1" href="uasdsk_contacts.php">Kontak</a></li>
				</ul>
			</div>
			<div class="content p">
				<div class="b">
					<H3>Curriculum Vitae</H3>
					<table>
						<tr>
							<td>Nama </td>
							<td>: Muhammad Fahmi</td>
						</tr>
						<tr>
							<td>Tempat & Tanggal Lahir</td>
							<td>: Bandung, 23 Februari 2004</td>
						</tr>
						<tr>
							<td>Agama</td>
							<td>: Islam</td>
						</tr>
						<tr>
							<td>Jenis kelamin</td>
							<td>: Laki-Laki</Laki-Laki></td>
						</tr>
						<tr>
							<td>Alamat </td>
							<td>: Jalan Sinar Bakti no.306, Perum. Sinar Waluyo, Semarang</td>
					
					</table>
				</div>
				<div class="b">
					<h3>Riwayat Pendidikan</h3>
					<table>
						<tr>
							<td>2010 - 2016</td>
							<td>: SD Negeri Sendangmulyo 04 Semarang</td>
						</tr>
						<tr>
							<td>2016 - 2019</td>
							<td>: SMP Islam Terpadu PAPB Semarang</td>
						</tr>
						<tr>
							<td>2019 - 2022</td>
							<td>: SMA Negeri 15 Semarang</td>
						</tr>

					</table>
				</div>
				<div class="b">
					<h3>Kegemaran</h3>
					<table>
						<tr>
							<td>Membaca Buku, Menulis, Mendengarkan Musik, Menonton Film</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<div class="footer">
		<?php
            echo "Muhammad Fahmi @2022 (footer ini dibuat dengan fitur PHP)";
        ?> 
		</div>
	
</body>
</html>