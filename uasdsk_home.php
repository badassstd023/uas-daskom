<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title></title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body class="body">
		<div class="header">
			<div class="container-header">
				<h1><b>F BLOG!</b></h1>
				<p><b>Welcome to Fahmi's blog</b></p>
				<div class="jam-digital-f">
					<div class="kotak">
						<p id="jam"></p>
					</div>
					<div class="kotak">
						<p id="menit"></p>
					</div>
					<div class="kotak">
						<p id="detik"></p>
					</div>
					<script>
						window.setTimeout("waktu()", 1000);
					 
						function waktu() {
							var waktu = new Date();
							setTimeout("waktu()", 1000);
							document.getElementById("jam").innerHTML = waktu.getHours();
							document.getElementById("menit").innerHTML = waktu.getMinutes();
							document.getElementById("detik").innerHTML = waktu.getSeconds();
						}
					</script>
				</div>
			</div>			
			<div class="container-photo">
				<img src="./foto jas almamater.jpg" alt="">
			</div>
			
			
		</div>
		<div class="badan">			
			<div class="sidebar">
				<h1>Menu</h1>
				<ul>
					<li><a class ="button1" href="uasdsk_home.php">Beranda</a></li>
					<li><a class ="button1" href="uasdsk_profile.php">Profil Akademik</a></li>
					<li><a class ="button1" href="uasdsk_cv.php">Curriculum Vitae</a></li>
					<li><a class ="button1" href="uasdsk_about.php">Tentang</a></li>		
					<li><a class ="button1" href="uasdsk_contacts.php">Kontak</a></li>
				</ul>
			</div>
			<div class="content p">
				<p>Sekilas berbicara mengenai kuliah program studi Teknik Informatika bagi orang awam yang tidak mengerti
					pasti akan mengira Teknik Informatika itu hanya sebuah ilmu yang mempelajari bagaimana sebuah perangkat keras dan lunak komputer bekerja.
					Pada kesempatan kali ini aku akan menceritakan bagaimana kuliahku di semester awal ini, aku menemukan banyak pengalaman menarik dan sudut pandang baru yang tentunya
					berbeda daripada orang kebanyakan. Pada mulanya aku memang masih terasa asing dengan jurusan Teknik Informatika dan sedikit dibayangi keraguan karena takut berjalan tidak sesuai
					dengan ekspektasiku.</p>
				<p>Pada akhirnya aku menyadari kalau menjadi mahasiswa Teknik Informatika itu tidak hanya berkutat pada coding dan komputer semata,
					tetapi kita juga diajarkan tentang bagaimana berpikir secara sistematis dan menggunakan logika secara tepat karena pada jurusan ini kita
					juga dituntut untuk teliti dan cermat. Karena perlu juga untuk kalian tahu bahwa sains tetaplah sains. Dan bagimana cara komputer bekerja itu juga merupakan bagian
					dari kekayaan sains. Yang tentunya pula di prodi ini kita tidak akan lepas dari hitungan, mulai dari Kalkulus, Matematika Diskrit, Logika, Algoritma akan kita jumpai semua.
					Pada akhirnya menjadi anak Informatika itu menyenangkan dan bisa dinikmati bagi yang terus mau belajar dan punya rasa ingin tahu yang tinggi.</p>
			</div>
		</div>
		<div class="clear"></div>
		<div class="footer">
		<?php
            echo "Muhammad Fahmi @2022 (footer ini dibuat dengan fitur PHP)";
        ?> 
		</div>
	
</body>
</html>