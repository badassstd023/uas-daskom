<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title></title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body class="body">
		<div class="header">
			<div class="container-header">
				<h1><b>F BLOG!</b></h1>
				<p><b>This is my Academic Profile</b></p>
				<div class="jam-digital-f">
					<div class="kotak">
						<p id="jam"></p>
					</div>
					<div class="kotak">
						<p id="menit"></p>
					</div>
					<div class="kotak">
						<p id="detik"></p>
					</div>
					<script>
						window.setTimeout("waktu()", 1000);
					 
						function waktu() {
							var waktu = new Date();
							setTimeout("waktu()", 1000);
							document.getElementById("jam").innerHTML = waktu.getHours();
							document.getElementById("menit").innerHTML = waktu.getMinutes();
							document.getElementById("detik").innerHTML = waktu.getSeconds();
						}
					</script>
				</div>
			</div>			
			<div class="container-photo">
				<img src="./foto jas almamater.jpg" alt="">
			</div>
			
			
		</div>
		<div class="badan">			
			<div class="sidebar">
				<h1>Menu</h1>
				<ul>
					<li><a class ="button1" href="uasdsk_home.php">Beranda</a></li>
					<li><a class ="button1" href="uasdsk_profile.php">Profil Akademik</a></li>
					<li><a class ="button1" href="uasdsk_cv.php">Curriculum Vitae</a></li>
					<li><a class ="button1" href="uasdsk_about.php">Tentang</a></li>		
					<li><a class ="button1" href="uasdsk_contacts.php">Kontak</a></li>
				</ul>
			</div>
			<div class="content">
				<table class="d" align="center" cellpadding="20" width="600px">
					<tr>
					  <td>Nama Lengkap</td>
					  <td>: Muhammad Fahmi</td>
					</tr>
					<tr>
					  <td>NIM</td>
					  <td>: A11.2022.14523</td>
					</tr>
					<tr>
					  <td>Kelompok</td>
					  <td>: A11.4114</td>
					</tr>
					<tr>
						<td>Dosen Wali</td>
						<td>: Liya Umaroh, M. Hum</td>
					</tr>
					<tr>
					  <td>Program Studi</td>
					  <td>: Teknik Informatika</td>
					</tr>
					<tr>
						<td>Fakultas</td>
						<td>: Ilmu Komputer</td>
					</tr>
					<tr>
					  <td>Kampus</td>
					  <td>: Universitas Dian Nuswantoro</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="clear"></div>
		<div class="footer">
		<?php
            echo "Muhammad Fahmi @2022 (footer ini dibuat dengan fitur PHP)";
        ?> 
		</div>
	
</body>
</html>